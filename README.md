## Description

App HnFeeds in Angular and Nestjs.

## Create DB in MongoDB Compass

Run the command below, then open your mongodb client (MongoDB Compass), then create the 'Feed' database and within it create the 'Feeds' collection.
Connection string: mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false

```bash
$ docker-compose up --build -d mongo
```

## Build image frontend

```bash
$ docker build -t hn-feeds-angular hn-feed-front/
```

## Build image backend

```bash
$ docker build -t hn-feeds-nestjs hn-feed-back/
```

## Running App with docker-compose

```bash
# development
$ docker-compose up
```

## Open App

```bash
# development
Navigate to `http://localhost:4200/`.
```
